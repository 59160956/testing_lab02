module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }
        if(username.toLowerCase() !== username){
            return false;
        }
        return true;
    },
    isAgeValid : function(age){
        if(isNaN(age)){
            return false;
        }
        if(parseInt(age)<18 || parseInt(age)>100){
            return false;
        }
        return true;
          
    },
    isPasswordValid : function(password){
        var Big = 0;
        var Num =0;
        var Epic =0;
        if(password.length < 8){
            return false;
        }
        for(i=0;i<password.length;i++){
            p = password.substring(i, i+1);
            if( p.search(/[^a-zA-Z0-9]/) !==-1 ){
                Epic++;
            }else if(isNaN(p)){
                if(p == p.toUpperCase()){
                    Big++;
                }
            }else{
                Num++;
            }
        }
        //console.log(Big+" "+Num+" "+Epic);
        if(Big<1){
            return false;
        }
        if(Num<3){
            return false;
        }
        if(Epic<1){
            return false;
        }
        return true;
    },
    isDateValid : function(day,month,year){
        if(day < 1 || day >31 ){
            return false;
        }
        if(month < 1 || month >12){
            return false;
        }
        if(year < 1970 || year > 2020){
            return false;
        }
        if((month == 4 && day > 30)||(month == 6 && day > 30)||(month == 9 && day > 30)||(month == 11 && day > 30)){
            return false;
        }
        if((year%400 == 0) && month == 2 && day>29){
            return false;
        }
        if((year%400 != 0) &&month == 2 && day > 28){
            return false;
        }
        return true;
    }
}
